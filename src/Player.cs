using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

class Player {
    private readonly Texture2D texture;
    private Vector2 position;
    private Vector2 scale;
    private Song fireSong;
    private Song dieSong;

    private float rotationAngle = 0f;
    private float jumpVelocity = 0f;
    private readonly float gravity = 0.5f;

    public Player(Texture2D playerTexture, Vector2 startingPosition,Song playerJumpEffect, Song playerFireEffect)
    {
        texture = playerTexture;
        position = startingPosition;
        scale = new Vector2(.5f,.5f);
        fireSong = playerJumpEffect;
        dieSong = playerFireEffect;
        MediaPlayer.Volume = 0.5f;
    }

    public void Update(GameTime gameTime, GraphicsDeviceManager graphics)
    {
        var kstate = Keyboard.GetState();
        int PLAYER_SPEED = 20;

        bool rightLimit = graphics.PreferredBackBufferWidth > position.X + texture.Width / 3;
        if (kstate.IsKeyDown(Keys.Right) && rightLimit) position.X += PLAYER_SPEED;

        bool leftLimit = texture.Width / 3 < position.X;
        if (kstate.IsKeyDown(Keys.Left) && leftLimit) position.X -= PLAYER_SPEED;

        Jump(kstate, graphics);
        LieDown(kstate);
    }

    private void Jump(KeyboardState state, GraphicsDeviceManager graphics) {
        int JUMP_SPEED = 10;

        if (state.IsKeyDown(Keys.Up) && Math.Abs(position.Y - (graphics.PreferredBackBufferHeight / 2)) < 1)
        {
            MediaPlayer.Play(fireSong);
            jumpVelocity = -JUMP_SPEED;
        }

        position.Y += jumpVelocity;
        jumpVelocity += gravity;

        float originalY = graphics.PreferredBackBufferHeight / 2;
        if (position.Y > originalY)
        {
            position.Y = originalY;
            jumpVelocity = 0;
        }
    }

    private void LieDown(KeyboardState state) {
        if (state.IsKeyDown(Keys.Down)) {
            MediaPlayer.Play(dieSong);
            return;
        }

        rotationAngle = 0;
    }

    public void Draw(SpriteBatch spriteBatch)
    {
        spriteBatch.Draw(
            texture,
            position,
            null,
            Color.White,
            rotationAngle,
            new Vector2(texture.Width / 2, -position.Y + texture.Height + 50),
            scale,
            SpriteEffects.None,
            0f
        );
    }
}